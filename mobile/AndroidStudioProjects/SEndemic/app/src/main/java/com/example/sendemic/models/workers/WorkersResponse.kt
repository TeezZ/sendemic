package com.example.sendemic.models.workers

data class WorkersResponse (
    val id: Int,
    val first_name: String,
    val last_name: String,
    val position: String
)