package com.example.sendemic.models.company

data class HealthGraphResponse (
    val HealthCompany : List<CompanyHealth>
)