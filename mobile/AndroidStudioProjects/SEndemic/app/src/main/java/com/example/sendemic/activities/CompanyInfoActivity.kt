package com.example.sendemic.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.sendemic.R
import com.example.sendemic.api.retrofit.RetrofitClient
import com.example.sendemic.models.company.HeadmasterCompanyResponse
import com.example.sendemic.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_company.*
import retrofit2.Call
import retrofit2.Response


class CompanyInfoActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company)

        btn_company_health_graph.setOnClickListener{
            val intent = Intent(applicationContext, CompanyGraphActivity::class.java)
            startActivity(intent)
        }

        val idHeadmaster = SharedPrefManager.getInstance(applicationContext).user.userId

        RetrofitClient.instance.getHeadmasterCompany(idHeadmaster)
        .enqueue(object: retrofit2.Callback<HeadmasterCompanyResponse>{
            override fun onFailure(call: Call<HeadmasterCompanyResponse>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(
                call: Call<HeadmasterCompanyResponse>,
                response: Response<HeadmasterCompanyResponse>
            ) {
                if(response.body()?.Indicators != null){
                    val body = response.body()?.Indicators

                    SharedPrefManager.getInstance(applicationContext).saveCompany(body!![0].id)

                    company_info_id.text = "ID: " + body[0].id.toString()
                    company_info_name.text = body[0].name
                    company_info_address.text = body[0].address
                    company_info_subscription_id.text = body[0].subscription_id.toString()

                } else {
                    Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        })

    }
}