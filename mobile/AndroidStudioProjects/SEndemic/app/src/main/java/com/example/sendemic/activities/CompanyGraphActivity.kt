package com.example.sendemic.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.example.sendemic.R
import com.example.sendemic.api.retrofit.RetrofitClient
import com.example.sendemic.models.company.HealthGraphResponse
import com.example.sendemic.models.user.ProfileResponse
import com.example.sendemic.storage.SharedPrefManager
import retrofit2.Call
import retrofit2.Response

class CompanyGraphActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_graph)
        var anyChartView: AnyChartView = findViewById(R.id.health_graph)

        var chart = AnyChart.line()
        var chartData = ArrayList<DataEntry>()

        val company_id = SharedPrefManager.getInstance(applicationContext).company.companyId

        RetrofitClient.instance.getCompanyHealthInfo(company_id)
            .enqueue(object: retrofit2.Callback<HealthGraphResponse>{
                override fun onFailure(call: Call<HealthGraphResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                }

                @SuppressLint("SetTextI18n")
                override fun onResponse(
                    call: Call<HealthGraphResponse>,
                    response: Response<HealthGraphResponse>
                ) {
                    if(response.body()?.HealthCompany != null){
                        val body = response.body()?.HealthCompany
                        val size = body?.count()

                        for(item in 0 until size!!){
                            chartData.add(ValueDataEntry(body[item].date, body[item].temperature))
                        }

                        chart.data(chartData)
                        anyChartView.setChart(chart)

                    } else {
                        Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_SHORT).show()
                    }
                }
            })
    }

}