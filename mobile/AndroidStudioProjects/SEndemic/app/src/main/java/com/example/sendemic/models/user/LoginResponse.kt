package com.example.sendemic.models.user

data class LoginResponse(val message: String, val token: String)