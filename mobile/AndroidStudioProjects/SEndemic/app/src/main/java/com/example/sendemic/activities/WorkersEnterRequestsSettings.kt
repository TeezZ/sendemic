package com.example.sendemic.activities

import android.app.*
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.sendemic.R
import com.example.sendemic.api.retrofit.RetrofitClient
import com.example.sendemic.models.health.AllowEnterResponse
import com.example.sendemic.models.health.UndeliveredEnterRequestResponse
import com.example.sendemic.models.workers.WorkersResponse
import com.example.sendemic.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_workers_requests_settings.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.concurrent.timerTask

class WorkersEnterRequestsSettings: AppCompatActivity(){

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channelId = "com.example.sendemic.activities"
    private val description = "Workers requests notification"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_workers_requests_settings)

        val companyId = SharedPrefManager.getInstance(applicationContext).company.companyId

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val intent = Intent(this, WorkersEnterRequestsSettings::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alertBuilder = AlertDialog.Builder(this)
        var isEmptyRequests = true

        fun getWorkersRequests(allow: Boolean){
            RetrofitClient.instance.getWorkersUndeliveredRequests(companyId)
                .enqueue(object: retrofit2.Callback<UndeliveredEnterRequestResponse>{
                    override fun onFailure(call: Call<UndeliveredEnterRequestResponse>, t: Throwable) {
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }
                    override fun onResponse(
                        call: Call<UndeliveredEnterRequestResponse>,
                        response: Response<UndeliveredEnterRequestResponse>
                    ) {
                        if(response.body()?.EnterRequests != null){
                            val body = response.body()?.EnterRequests
                            val size = body?.count()

                            isEmptyRequests = size!! <= 0
                            fun enterRequest(allow: Boolean){
                                RetrofitClient.instance.allowingWorkerEnter(
                                    body[0].id,
                                    true,
                                    allow
                                )
                                    .enqueue(object : retrofit2.Callback<AllowEnterResponse> {
                                        override fun onFailure(
                                            call: Call<AllowEnterResponse>,
                                            t: Throwable
                                        ) {
                                            Toast.makeText(
                                                applicationContext,
                                                t.message,
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }

                                        override fun onResponse(
                                            call: Call<AllowEnterResponse>,
                                            response: Response<AllowEnterResponse>
                                        ) {
                                            if (response.body()?.message != null) {
                                                getWorkersRequests(allow)
                                            } else {
                                                Toast.makeText(
                                                    applicationContext,
                                                    "Cannot send allow request",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }
                                        }
                                    })
                            }

                            if(size > 0) {
                               enterRequest(allow)
                            }
                        } else {
                            Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_SHORT).show()
                        }
                    }
                })

        }

        fun getWorkersRequests(){
            RetrofitClient.instance.getWorkersUndeliveredRequests(companyId)
                .enqueue(object: retrofit2.Callback<UndeliveredEnterRequestResponse>{
                    override fun onFailure(call: Call<UndeliveredEnterRequestResponse>, t: Throwable) {
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }
                    override fun onResponse(
                        call: Call<UndeliveredEnterRequestResponse>,
                        response: Response<UndeliveredEnterRequestResponse>
                    ) {
                        if(response.body()?.EnterRequests != null){
                            val body = response.body()?.EnterRequests
                            val size = body?.count()

                            isEmptyRequests = size!! <= 0

                            if(size > 0) {
                                alertBuilder.setTitle("Request #${body[0].id}.\nWorker ${body[0].first_name} ${body[0].last_name} \nwant to enter")
                                alertBuilder.setMessage("Do you want allow this worker enter?\nWorker position: ${body[0].position}")

                                alertBuilder.setPositiveButton("Allow") { dialogInterface: DialogInterface, i: Int ->

                                    RetrofitClient.instance.allowingWorkerEnter(
                                        body[0].id,
                                        true,
                                        true
                                    )
                                        .enqueue(object : retrofit2.Callback<AllowEnterResponse> {
                                            override fun onFailure(
                                                call: Call<AllowEnterResponse>,
                                                t: Throwable
                                            ) {
                                                Toast.makeText(
                                                    applicationContext,
                                                    t.message,
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }

                                            override fun onResponse(
                                                call: Call<AllowEnterResponse>,
                                                response: Response<AllowEnterResponse>
                                            ) {
                                                if (response.body()?.message != null) {
                                                    Toast.makeText(
                                                        applicationContext,
                                                        "Allowed",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                    getWorkersRequests()
                                                } else {
                                                    Toast.makeText(
                                                        applicationContext,
                                                        "Something went wrong",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                }
                                            }
                                        })
                                }
                                alertBuilder.setNegativeButton("Deny") { dialogInterface: DialogInterface, i: Int ->
                                    RetrofitClient.instance.allowingWorkerEnter(
                                        body[0].id,
                                        true,
                                        false
                                    )
                                        .enqueue(object : retrofit2.Callback<AllowEnterResponse> {
                                            override fun onFailure(
                                                call: Call<AllowEnterResponse>,
                                                t: Throwable
                                            ) {
                                                Toast.makeText(
                                                    applicationContext,
                                                    t.message,
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }

                                            override fun onResponse(
                                                call: Call<AllowEnterResponse>,
                                                response: Response<AllowEnterResponse>
                                            ) {
                                                if (response.body()?.message != null) {
                                                    Toast.makeText(
                                                        applicationContext,
                                                        "Denied",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                    getWorkersRequests()
                                                } else {
                                                    Toast.makeText(
                                                        applicationContext,
                                                        "Something went wrong",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                }
                                            }
                                        })
                                }
                                alertBuilder.setCancelable(false)
                                alertBuilder.show()


                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    notificationChannel = NotificationChannel(
                                        channelId,
                                        description,
                                        NotificationManager.IMPORTANCE_HIGH
                                    )
                                    notificationChannel.enableLights(true)
                                    notificationChannel.lightColor = Color.CYAN
                                    notificationChannel.enableVibration(true)
                                    notificationManager.createNotificationChannel(
                                        notificationChannel
                                    )

                                    builder = Notification.Builder(applicationContext, channelId)
                                        .setContentTitle("Worker want to enter")
                                        .setContentText("Allow worker to enter?")
                                        .setSmallIcon(R.mipmap.ic_launcher_round)
                                        .setContentIntent(pendingIntent)
                                } else {
                                    builder = Notification.Builder(applicationContext)
                                        .setContentTitle("Worker want to enter")
                                        .setContentText("Allow worker to enter?")
                                        .setSmallIcon(R.mipmap.ic_launcher_round)
                                        .setContentIntent(pendingIntent)
                                }

                                if (body!![0].id != null) {
                                    notificationManager.notify(body!![0].id, builder.build())
                                }

                            }
                        } else {
                            Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_SHORT).show()
                        }
                    }
                })

        }

        val radioShowEnter: RadioButton = findViewById(R.id.radio_show_all_enter)
        val radioAllowEnter: RadioButton = findViewById(R.id.radio_alow_all_enter)
        val radioDenyEnter: RadioButton = findViewById(R.id.radio_deny_all_enter)

        var mainHandler = Handler(Looper.getMainLooper())

        val updateRequestsTask = object: Runnable {
            override fun run() {
                if(isEmptyRequests){
                    if(radioShowEnter.isChecked){
                        getWorkersRequests()
                    }else if(radioAllowEnter.isChecked){
                        getWorkersRequests(true)
                    }else if(radioDenyEnter.isChecked){
                        getWorkersRequests(false)
                    }
                }
                mainHandler.postDelayed(this, 3000)
            }
        }
        mainHandler.post(updateRequestsTask)

//        radio_group_enter.setOnCheckedChangeListener(
//            RadioGroup.OnCheckedChangeListener{group, checkedId ->
//                var mainHandler = Handler(Looper.getMainLooper())
//                val radio: RadioButton = findViewById(checkedId)
//
//                when(radio.id){
//                    R.id.radio_show_all_enter -> {
//                        if(radioShowEnter.isChecked){
//                            Toast.makeText(
//                                applicationContext,
//                                "Show all requests!",
//                                Toast.LENGTH_SHORT
//                            ).show()
//                            val updateRequestsTask = object: Runnable {
//                                override fun run() {
//                                    if(isEmptyRequests){
//                                        getWorkersRequests()
//                                    }
//                                    mainHandler.postDelayed(this, 3000)
//                                }
//                            }
//                            mainHandler.post(updateRequestsTask)
//                        }
//                    }
//                    R.id.radio_alow_all_enter -> {
//                        if(radioAllowEnter.isChecked){
//                            Toast.makeText(
//                                applicationContext,
//                                "Allow all !",
//                                Toast.LENGTH_SHORT
//                            ).show()
//                            val updateRequestsTask = object: Runnable {
//                                override fun run() {
//                                    if(isEmptyRequests){
//                                        getWorkersRequests(true)
//                                    }
//                                    mainHandler.postDelayed(this, 3000)
//                                }
//                            }
//                            mainHandler.post(updateRequestsTask)
//                        }
//                    }
//                    R.id.radio_deny_all_enter -> {
//                        if(radioDenyEnter.isChecked){
//                            Toast.makeText(
//                                applicationContext,
//                                "Deny all !",
//                                Toast.LENGTH_SHORT
//                            ).show()
//                            val updateRequestsTask = object: Runnable {
//                                override fun run() {
//                                    if(isEmptyRequests){
//                                        getWorkersRequests(false)
//                                    }
//                                    mainHandler.postDelayed(this, 3000)
//                                }
//                            }
//                            mainHandler.post(updateRequestsTask)
//                        }
//                    }
//                }
//            }
//        )

    }

}