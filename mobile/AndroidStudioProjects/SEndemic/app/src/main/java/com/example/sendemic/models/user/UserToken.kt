package com.example.sendemic.models.user

data class UserToken(val token: String)
