package com.example.sendemic.models.company

data class CompanyHealth(
    val date: String,
    val temperature: Float
)