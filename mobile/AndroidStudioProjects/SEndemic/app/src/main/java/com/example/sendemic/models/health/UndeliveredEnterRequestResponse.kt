package com.example.sendemic.models.health

data class UndeliveredEnterRequestResponse(
    val EnterRequests : List<EnterRequest>
)