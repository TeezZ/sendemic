package com.example.sendemic.models.health

data class AllowEnterResponse (
    val message: String
)