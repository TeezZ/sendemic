package com.example.sendemic.models.user

data class ProfileResponse (
    val id: Int,
    val first_name: String,
    val last_name: String,
    val email: String
)