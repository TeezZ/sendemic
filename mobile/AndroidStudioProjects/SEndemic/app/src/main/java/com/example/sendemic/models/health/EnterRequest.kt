package com.example.sendemic.models.health

data class EnterRequest (
    val id: Int,
    val worker_id: Int,
    val date: String,
    val is_allowed: Number,
    val is_delivered: Number,
    val first_name: String,
    val last_name: String,
    val position: String
)