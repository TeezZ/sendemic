package com.example.sendemic.api.retrofit

import com.example.sendemic.models.company.HeadmasterCompanyResponse
import com.example.sendemic.models.company.HealthGraphResponse
import com.example.sendemic.models.health.AllowEnterResponse
import com.example.sendemic.models.health.EnterRequest
import com.example.sendemic.models.health.UndeliveredEnterRequestResponse
import com.example.sendemic.models.user.LoginResponse
import com.example.sendemic.models.user.ProfileResponse
import com.example.sendemic.models.workers.WorkersResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST


interface IRetrofitApi {
    @FormUrlEncoded
    @POST("headmasters/login")
    fun userLogin(@Field("email") email:String,
                  @Field("password") password:String
    ): Call<LoginResponse>

    @GET("headmasters/getHeadmasterProfile")
    fun userProfile():Call<ProfileResponse>

    @FormUrlEncoded
    @POST("headmasters/getHeadmasterCompany")
    fun getHeadmasterCompany(@Field("id_headmaster") id_headmaster:Int
    ): Call<HeadmasterCompanyResponse>

    @FormUrlEncoded
    @POST("healthindicators/getCompanyHealthIndicators")
    fun getCompanyHealthInfo(@Field("company_id") company_id:Int
    ): Call<HealthGraphResponse>

    @FormUrlEncoded
    @POST("enterRequests/getWorkersUndeliveredRequests")
    fun getWorkersUndeliveredRequests(@Field("company_id") company_id:Int
    ): Call<UndeliveredEnterRequestResponse>

    @FormUrlEncoded
    @POST("enterRequests/allowEnter")
    fun allowingWorkerEnter(@Field("enter_request_id") enter_request_id:Int,
                  @Field("is_delivered") is_delivered:Boolean,
                  @Field("is_allowed") is_allowed:Boolean
    ): Call<AllowEnterResponse>

    @FormUrlEncoded
    @POST("enterRequests/getWorkersUndeliveredRequests")
    fun getWorkersById(@Field("id_worker") id_worker:Int
    ): Call<WorkersResponse>

}