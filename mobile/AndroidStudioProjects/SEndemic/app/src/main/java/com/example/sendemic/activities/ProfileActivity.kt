package com.example.sendemic.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.sendemic.R
import com.example.sendemic.api.retrofit.RetrofitClient
import com.example.sendemic.models.user.ProfileResponse
import com.example.sendemic.storage.SharedPrefManager
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_profile.*
import retrofit2.Call
import retrofit2.Response

class ProfileActivity : AppCompatActivity() {

    var compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

//        btn_profile_vaccination_story.setOnClickListener {
//            val intent = Intent(applicationContext, VaccinationStoryActivity::class.java)
//            startActivity(intent)
//        }
//
        btn_profile_company_info.setOnClickListener {
            val intent = Intent(applicationContext, CompanyInfoActivity::class.java)
            startActivity(intent)
        }

      btn_worker_request_settings.setOnClickListener {
            val intent = Intent(applicationContext, WorkersEnterRequestsSettings::class.java)
            startActivity(intent)
        }

        RetrofitClient.instance.userProfile()
            .enqueue(object: retrofit2.Callback<ProfileResponse>{
                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {
                    if(response.body()?.id != null){
                        SharedPrefManager.getInstance(applicationContext).saveUser(response.body()?.id!!)
                        profile_user_id.text = "ID: " + response.body()?.id.toString()
                        profile_user_first_name.text = response.body()?.first_name
                        profile_user_last_name.text = response.body()?.last_name
                        profile_user_email.text = response.body()?.email
                    } else {
                        Toast.makeText(applicationContext, "Something went wrong. Try to login later", Toast.LENGTH_SHORT).show()
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                    }
                }
            })
    }

    override fun onStop() {
        compositeDisposable.clear();
        super.onStop()
    }

    override fun onDestroy() {
        compositeDisposable.clear();
        super.onDestroy()
    }

}