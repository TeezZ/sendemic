package com.example.sendemic.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.sendemic.R
import com.example.sendemic.api.retrofit.RetrofitClient
import com.example.sendemic.models.user.LoginResponse
import com.example.sendemic.storage.SharedPrefManager
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    var compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        login_button.setOnClickListener {
            val email = login_email.text.toString().trim()
            val password = login_password.text.toString().trim()

            if(email.isEmpty()){
                login_email.error = "Email is required"
                login_email.requestFocus()
                return@setOnClickListener
            }

            if(password.isEmpty()){
                login_password.error = "Password is required"
                login_password.requestFocus()
                return@setOnClickListener
            }

            RetrofitClient.instance.userLogin(email, password)
                .enqueue(object: retrofit2.Callback<LoginResponse>{
                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(
                        call: Call<LoginResponse>,
                        response: Response<LoginResponse>
                    ) {
                        if(response.body()?.token != null){
                            SharedPrefManager.getInstance(applicationContext).saveUserToken(response.body()?.token.toString())

                            RetrofitClient.AUTH = SharedPrefManager.getInstance(applicationContext).userToken.token

                            val intent = Intent(applicationContext, ProfileActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                        } else {
                            Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_SHORT).show()
                        }
                    }
                })
        }
    }

    override fun onStop() {
        compositeDisposable.clear();
        super.onStop()
    }

    override fun onDestroy() {
        compositeDisposable.clear();
        super.onDestroy()
    }
}
