package com.example.sendemic.models.company

import java.util.*

data class Companies (
    val id: Int,
    val name: String,
    val address: String,
    val subscription_id: Int,
    val subscription_start_date: String,
    val subscription_end_date: String
)