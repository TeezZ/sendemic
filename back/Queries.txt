/* Headmaster */

headmasters/
{
    "first_name": "Test",
    "last_name": "Testname",
    "email": "head01@test.ua",
    "password": "head01"
}

headmasters/login
{
    "email": "head01@test.ua",
    "password": "head01"
}

headmasters/getHeadmasters

headmasters/getHeadmasterProfile

headmasters/getHeadmasterById
{
    "id_headmaster": "1"
}

headmasters/getHeadmasterCompany
{
    "id_headmaster": "1"
}


/* Company */

companies/
{
    "headmaster_id": "1",
    "name": "Test Company",
    "address": "Lorem city 255",
    "subscription_id": "1",
    "subscription_start": "04/17/2020",
    "subscription_end": "06/17/2020"
}

companies/getCompanies

companies/getCompanyById
{
    "id_company": "1"
}

companies/getCompanyByName
{
    "name": "Test"
}


/* Worker */

workers/
{
    "company_id": "1",
    "first_name": "Lorem",
    "last_name": "Ipsum",
    "position": "Manager"
}

workers/getWorkers

workers/getWorkerById
{
    "id_worker": "1"
}

workers/getWorkerByName
{
    "last_name": "Ipsum"
}

workers/getWorkerByPosition
{
    "position": "manager"
}


/* Health indicators */

healthindicators/
{
    "date": "04/17/2020",
    "temperature": "36",
    "saturation": "98",
    "id_worker": "1"
}

healthindicators/getUserHealthIndicators
{
    "id_worker": "1"
}