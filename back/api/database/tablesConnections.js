const Company = require("../models/Company");
const Headmaster = require("../models/Headmaster");
const HeadmasterCompany = require("../models/HeadmasterCompany");
const HealthIndicator = require("../models/HealthIndicator");
const Subscription = require("../models/Subscription");
const Worker = require("../models/Worker");
const WorkerIndicator = require("../models/WorkerIndicator");


module.exports = () => {
    Worker.hasMany(WorkerIndicator, { onDelete: "cascade", foreignKey: "fk_worker_id", sourceKey: "id" });
    HealthIndicator.hasMany(WorkerIndicator, { onDelete: "cascade", foreignKey: "fk_indicator_id", sourceKey: "id" });

    Headmaster.hasMany(HeadmasterCompany, { onDelete: "cascade", foreignKey: "fk_headmaster_id", sourceKey: "id" });
    Company.hasMany(HeadmasterCompany, { onDelete: "cascade", foreignKey: "fk_company_id", sourceKey: "id" });

    Company.hasMany(Worker, { onDelete: "cascade" });
    Subscription.hasMany(Company, { onDelete: "cascade" });
}