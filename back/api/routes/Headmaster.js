const express = require("express");
const router = express.Router();
const HeadmasterController = require('../controllers/Headmaster');

router.post("/", HeadmasterController.Headmaster_create);
router.post("/login",  HeadmasterController.Headmaster_login);
router.get("/getHeadmasters", verifyToken, HeadmasterController.Headmaster_getAll);
router.get("/getHeadmasterProfile", verifyToken, HeadmasterController.Headmaster_getProfile);
router.post("/getHeadmasterById", verifyToken, HeadmasterController.Headmaster_getHeadmasterById);
router.post("/getHeadmasterCompany", verifyToken, HeadmasterController.Headmaster_getHeadmasterCompany);

function verifyToken(req, res, next){
    //Get auth header value
    const bearerHeader = req.headers["authorization"];
    
    if(typeof bearerHeader !== 'undefined'){
       //Split the form of token
       const bearer = bearerHeader.split(' '); // Bearer - {0}, <access_token> - {1}
       const bearerToken = bearer[1];
       req.token = bearerToken;
       next();
    } else {
       res.sendStatus(403);
    }
}

module.exports = router;