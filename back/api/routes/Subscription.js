const express = require("express");
const router = express.Router();
const SubscriptionController = require('../controllers/Subscription');

router.post("/", verifyToken, SubscriptionController.Subscription_create);
router.get("/getSubscriptions", SubscriptionController.Subscription_getAll);
router.post("/getSubscriptionById", SubscriptionController.Subscription_getSubscriptionById);
router.post("/getSubscriptionByName", SubscriptionController.Subscription_getSubscriptionByName);

function verifyToken(req, res, next){
    //Get auth header value
    const bearerHeader = req.headers["authorization"];
    
    if(typeof bearerHeader !== 'undefined'){
       //Split the form of token
       const bearer = bearerHeader.split(' '); // Bearer - {0}, <access_token> - {1}
       const bearerToken = bearer[1];
       req.token = bearerToken;
       next();
    } else {
       res.sendStatus(403);
    }
}

module.exports = router;