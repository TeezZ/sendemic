const express = require("express");
const router = express.Router();
const AdministratorController = require('../controllers/Administrator');

router.post("/", AdministratorController.Administrator_create);
router.post("/login",  AdministratorController.Administrator_login);
router.get("/getAdministrators", verifyToken, AdministratorController.Administrator_getAll);
router.get("/getAdministratorProfile", verifyToken, AdministratorController.Administrator_getProfile);
router.post("/getAdministratorById", verifyToken, AdministratorController.Administrator_getAdministratorById);

function verifyToken(req, res, next){
    //Get auth header value
    const bearerHeader = req.headers["authorization"];
    
    if(typeof bearerHeader !== 'undefined'){
       //Split the form of token
       const bearer = bearerHeader.split(' '); // Bearer - {0}, <access_token> - {1}
       const bearerToken = bearer[1];
       req.token = bearerToken;
       next();
    } else {
       res.sendStatus(403);
    }
}

module.exports = router;