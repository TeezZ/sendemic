const express = require("express");
const router = express.Router();
const HealthIndicatorController = require("../controllers/HealthIndicator");

router.post("/", verifyToken, HealthIndicatorController.HealthIndicator_create);
router.post("/getUserHealthIndicators", verifyToken, HealthIndicatorController.HealthIndicator_getByWorkerId);

function verifyToken(req, res, next){
    //Get auth header value
    const bearerHeader = req.headers["authorization"];
    
    if(typeof bearerHeader !== 'undefined'){
       //Split the form of token
       const bearer = bearerHeader.split(' '); // Bearer - {0}, <access_token> - {1}
       const bearerToken = bearer[1];
       req.token = bearerToken;
       next();
    } else {
       res.sendStatus(403);
    }
}


module.exports = router;