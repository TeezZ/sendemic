const express = require("express");
const router = express.Router();
const CompanyController = require('../controllers/Company');

router.post("/", verifyToken, CompanyController.Company_create);
router.get("/getCompanies", CompanyController.Company_getAll);
router.post("/getCompanyById", CompanyController.Company_getCompanyById);
router.post("/getCompanyByName", CompanyController.Company_getCompanyByName);

function verifyToken(req, res, next){
    //Get auth header value
    const bearerHeader = req.headers["authorization"];
    
    if(typeof bearerHeader !== 'undefined'){
       //Split the form of token
       const bearer = bearerHeader.split(' '); // Bearer - {0}, <access_token> - {1}
       const bearerToken = bearer[1];
       req.token = bearerToken;
       next();
    } else {
       res.sendStatus(403);
    }
}
module.exports = router;