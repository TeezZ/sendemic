const express = require("express");
const router = express.Router();
const WorkerController = require('../controllers/Worker');

router.post("/", verifyToken, WorkerController.Worker_create);
router.get("/getWorkers", verifyToken, WorkerController.Worker_getAll);
router.post("/getWorkerById", verifyToken, WorkerController.Worker_getWorkerById);
router.post("/getWorkerByName", verifyToken, WorkerController.Worker_getWorkerByName);
router.post("/getWorkerByPosition", verifyToken, WorkerController.Worker_getWorkerByPosition);

function verifyToken(req, res, next){
    //Get auth header value
    const bearerHeader = req.headers["authorization"];
    
    if(typeof bearerHeader !== 'undefined'){
       //Split the form of token
       const bearer = bearerHeader.split(' '); // Bearer - {0}, <access_token> - {1}
       const bearerToken = bearer[1];
       req.token = bearerToken;
       next();
    } else {
       res.sendStatus(403);
    }
}

module.exports = router;