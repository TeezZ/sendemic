const Sequelize = require('sequelize');

require('../database/connection');

module.exports = sequelize.define("company", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    address: {
        type: Sequelize.STRING,
        allowNull: false
    },
    subscription_id: {
        type: Sequelize.INTEGER,
        model: 'subscription',
        key: 'id',
        allowNull: false
    },
    subscription_start_date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    subscription_end_date: {
        type: Sequelize.DATE,
        allowNull: false
    }
});