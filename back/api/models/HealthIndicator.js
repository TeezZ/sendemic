const Sequelize = require('sequelize');

require('../database/connection');

module.exports = sequelize.define("health_indicator", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    temperature: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    saturation: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});