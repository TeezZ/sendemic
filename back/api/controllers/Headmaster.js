const Headmaster = require("../models/Headmaster");
const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);

exports.Headmaster_getAll =  (req, res, next) => {
    Headmaster.findAll({ raw: true }).then(headmasters => {
        res.status(200).json({
            allHeadmasters: headmasters
        });
    });
};

exports.Headmaster_login = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    Headmaster.findAll({ where: { email: email }, raw: true })
        .then(headmaster => {
            if (headmaster.length < 1) {
                return res.status(401).json({
                    message: "Auth failed"
                });
            }
            bcrypt.compare(password, headmaster[0].passwordHash, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: "Auth failed"
                    });
                }
                if (result) {
                    const token = jwt.sign({ headmaster }, "secretKey", { expiresIn: '1h' });
                    return res.status(200).json({
                        message: "Auth successful",
                        token: token
                    })
                }
                res.status(401).json({
                    message: "Auth failed"
                })
            });
        }).catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
}

exports.Headmaster_create = (req, res, next) => {
    const firstName = req.body.first_name;
    const lastName = req.body.last_name;
    const email = req.body.email;
    const password = req.body.password;

    Headmaster.findAll({ where: { email: email }, raw: true })
        .then(headmaster => {
            if (headmaster.length >= 1) {
                return res.status(409).json({
                    message: "Mail already exists"
                });
            } else {
                Headmaster.create({
                    first_name: firstName,
                    last_name: lastName,
                    email: email,
                    passwordHash: bcrypt.hashSync(password, salt),
                }).then(result => {
                    console.log(result);
                    res.status(201).json({ message: "Headmaster created!" });
                }
                ).catch(err => {
                    console.log(err);
                    res.status(500).json({ error: err });
                });
            }
        })
        .catch()
};

exports.Headmaster_getProfile = (req, res, next)=>{
    jwt.verify(req.token, 'secretKey', (err, authData)=>{
      if(err){
        console.log(err);
        res.sendStatus(403);
      } else{
          if(!req.body) return res.sendStatus(400);

          const idCurrentHeadmaster = authData.headmaster[0].id;

          Headmaster.findAll({where:{id: idCurrentHeadmaster}, raw: true})
          .then((headmaster)=>{
            return res.status(200).json({
              id: headmaster[0].id,
              first_name: headmaster[0].first_name,
              last_name: headmaster[0].last_name,
              email: headmaster[0].email
            })
          })
          .catch(err=>{
            console.log(err);
            res.status(500).json({error: err});
        })
      }
    });
};

exports.Headmaster_getHeadmasterById = (req, res, next)=>{ 
    const id_headmaster = req.body.id_headmaster;

    Headmaster.findAll({where:{id: id_headmaster}, raw: true})
    .then((headmaster)=>{
      return res.status(200).json({
        id: headmaster[0].id_headmaster,
        first_name: headmaster[0].first_name,
        last_name: headmaster[0].last_name,
        email: headmaster[0].email
      })
    })
    .catch(err=>{
      console.log(err);
      res.status(500).json({error: err});
  })

};

exports.Headmaster_getHeadmasterCompany = (req, res, next)=>{ 
    const headmasterId = req.body.id_headmaster;

    try {
        let query = 'SELECT * FROM companies WHERE id IN (SELECT fk_company_id FROM headmaster_companies WHERE fk_headmaster_id = ' + headmasterId + ') ORDER BY id DESC';
        sequelize
            .query(query, {
                type: sequelize.QueryTypes.SELECT
            })
            .then(row => {
                res.status(200).json({
                    Indicators: row
                });
            })
    }
    catch (err) {
        res.status(400).json({
            message: err
        });
        console.log(err);
    }

};