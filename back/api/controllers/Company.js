const Company = require('../models/Company');
const HeadmasterCompany = require('../models/HeadmasterCompany');

exports.Company_create = (req, res, next) => {
    const headmasterId = req.body.headmaster_id;
    const name = req.body.name;
    const address = req.body.address;
    const subscriptionId = req.body.subscription_id;
    const subscriptionStart = req.body.subscription_start;
    const subscriptionEnd = req.body.subscription_end;

    Company.findAll({ where: { address: address }, raw: true })
        .then(company => {
            if (company.length >= 1) {
                return res.status(409).json({
                    message: "Company already exists"
                });
            } else {
                Company.create({
                    name: name,
                    address: address,
                    subscription_id: subscriptionId,
                    subscription_start_date: subscriptionStart,
                    subscription_end_date: subscriptionEnd
                }).then(result => {
                    console.log(result);
                    Company.findAll({
                        where:
                        {
                            address: address,
                            name: name
                        }, raw: true
                    }).then(company => {
                        HeadmasterCompany.create({
                            fk_headmaster_id: headmasterId,
                            fk_company_id: company[0].id
                        }).then(result => {
                            console.log(result);
                            res.status(201).json({ message: "Company created!" });
                        })
                    })
                }
                ).catch(err => {
                    console.log(err);
                    res.status(500).json({ error: err });
                });
            }
        })
        .catch()
};

exports.Company_getAll = (req, res, next) => {
    Company.findAll({ raw: true }).then(companies => {
        res.status(200).json({
            allCompanies: companies
        });
    });
};

exports.Company_getCompanyByName = (req, res, next) => {
    const name = req.body.name;

    try {
        let query = 'SELECT id, name, address FROM companies WHERE name LIKE "%' + name + '%"';
        sequelize
            .query(query, {
                type: sequelize.QueryTypes.SELECT
            })
            .then(row => {
                res.status(200).json({
                    Companies: row
                });
            })
    }
    catch (err) {
        res.status(400).json({
            message: err
        });
        console.log(err);
    }
};

exports.Company_getCompanyById = (req, res, next) => {
    const idCompany = req.body.id_company;

    Company.findAll({ where: { id: idCompany }, raw: true })
        .then((company) => {
            return res.status(200).json({
                id: company[0].idCompany,
                name: company[0].name,
                address: company[0].address,
                subscription_id: company[0].subscription_id,
                subscription_start_date: company[0].subscription_start_date,
                subscription_end_date: company[0].subscription_end_date
            })
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        })

};