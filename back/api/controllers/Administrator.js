const Administrator = require("../models/Administrator");
const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);

exports.Administrator_getAll =  (req, res, next) => {
    Administrator.findAll({ raw: true }).then(administrators => {
        res.status(200).json({
            allAdministrators: administrators
        });
    });
};

exports.Administrator_login = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    Administrator.findAll({ where: { email: email }, raw: true })
        .then(administrator => {
            if (administrator.length < 1) {
                return res.status(401).json({
                    message: "Auth failed"
                });
            }
            bcrypt.compare(password, administrator[0].passwordHash, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: "Auth failed"
                    });
                }
                if (result) {
                    const token = jwt.sign({ administrator }, "secretKey", { expiresIn: '1h' });
                    return res.status(200).json({
                        message: "Auth successful",
                        token: token
                    })
                }
                res.status(401).json({
                    message: "Auth failed"
                })
            });
        }).catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
}

exports.Administrator_create = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    Administrator.findAll({ where: { email: email }, raw: true })
        .then(administrator => {
            if (administrator.length >= 1) {
                return res.status(409).json({
                    message: "Mail already exists"
                });
            } else {
                Administrator.create({
                    email: email,
                    passwordHash: bcrypt.hashSync(password, salt),
                }).then(result => {
                    console.log(result);
                    res.status(201).json({ message: "Administrator created!" });
                }
                ).catch(err => {
                    console.log(err);
                    res.status(500).json({ error: err });
                });
            }
        })
        .catch()
};

exports.Administrator_getProfile = (req, res, next)=>{
    jwt.verify(req.token, 'secretKey', (err, authData)=>{
      if(err){
        console.log(err);
        res.sendStatus(403);
      } else{
          if(!req.body) return res.sendStatus(400);

          const idCurrentAdministrator = authData.administrator[0].id;

          Administrator.findAll({where:{id: idCurrentAdministrator}, raw: true})
          .then((administrator)=>{
            return res.status(200).json({
              id: administrator[0].id,
              email: administrator[0].email
            })
          })
          .catch(err=>{
            console.log(err);
            res.status(500).json({error: err});
        })
      }
    });
};

exports.Administrator_getAdministratorById = (req, res, next)=>{ 
    const id_administrator = req.body.id_administrator;

    Administrator.findAll({where:{id: id_administrator}, raw: true})
    .then((administrator)=>{
      return res.status(200).json({
        id: administrator[0].id_administrator,
        email: administrator[0].email
      })
    })
    .catch(err=>{
      console.log(err);
      res.status(500).json({error: err});
  })

};