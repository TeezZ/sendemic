const Worker = require("../models/Worker");

exports.Worker_getAll = (req, res, next) => {
    Worker.findAll({ raw: true }).then(workers => {
        res.status(200).json({
            allWorkers: workers
        });
    });
};

exports.Worker_create = (req, res, next) => {
    const firstName = req.body.first_name;
    const lastName = req.body.last_name;
    const position = req.body.position;
    const companyId = req.body.company_id;

    Worker.create({
        company_id: companyId,
        first_name: firstName,
        last_name: lastName,
        position: position
    }).then(result => {
        console.log(result);
        res.status(201).json({ message: "Worker created!" });
    }
    ).catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });
};

exports.Worker_getWorkerById = (req, res, next) => {
    const idWorker = req.body.id_worker;

    Worker.findAll({ where: { id: idWorker }, raw: true })
        .then((worker) => {
            return res.status(200).json({
                id: worker[0].id,
                first_name: worker[0].first_name,
                last_name: worker[0].last_name,
                position: worker[0].position
            })
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        })
};

exports.Worker_getWorkerByName = (req, res, next) => {
    const lastName = req.body.last_name;

    try {
        let query = 'SELECT id, first_name, last_name, position FROM workers WHERE last_name LIKE "%' + lastName + '%"';
        sequelize
            .query(query, {
                type: sequelize.QueryTypes.SELECT
            })
            .then(row => {
                res.status(200).json({
                    Workers: row
                });
            })
    }
    catch (err) {
        res.status(400).json({
            message: err
        });
        console.log(err);
    }
};

exports.Worker_getWorkerByPosition = (req, res, next) => {
    const position = req.body.position;

    try {
        let query = 'SELECT id, first_name, last_name, position FROM workers WHERE position LIKE "%' + position + '%"';
        sequelize
            .query(query, {
                type: sequelize.QueryTypes.SELECT
            })
            .then(row => {
                res.status(200).json({
                    Workers: row
                });
            })
    }
    catch (err) {
        res.status(400).json({
            message: err
        });
        console.log(err);
    }
};