const Subscription = require('../models/Subscription');

exports.Subscription_create = (req, res, next) => {
    const name = req.body.name;
    const description = req.body.description;
    const cost = req.body.cost;
    const duration = req.body.duration;
    
    Subscription.findAll({ where: { name: name }, raw: true })
        .then(company => {
            if (company.length >= 1) {
                return res.status(409).json({
                    message: "Subscription already exists"
                });
            } else {
                Subscription.create({
                    name: name,
                    description: description,
                    cost: cost,
                    duration: duration
                }).then(result => {
                    console.log(result);
                    res.status(201).json({ message: "Subscription created!" });
                }
                ).catch(err => {
                    console.log(err);
                    res.status(500).json({ error: err });
                });
            }
        })
        .catch()
};

exports.Subscription_getAll = (req, res, next) => {
    Subscription.findAll({ raw: true }).then(subscriptions => {
        res.status(200).json({
            allSubscriptions: subscriptions
        });
    });
};

exports.Subscription_getSubscriptionByName = (req, res, next) => {
    const name = req.body.name;

    try {
        let query = 'SELECT id, name, description, cost, duration FROM subscriptions WHERE name LIKE "%' + name + '%"';
        sequelize
            .query(query, {
                type: sequelize.QueryTypes.SELECT
            })
            .then(row => {
                res.status(200).json({
                    subscriptions: row
                });
            })
    }
    catch (err) {
        res.status(400).json({
            message: err
        });
        console.log(err);
    }
};

exports.Subscription_getSubscriptionById = (req, res, next)=>{ 
    const idSubscription = req.body.id_subscription;

    Subscription.findAll({where:{id: idSubscription}, raw: true})
    .then((subscription)=>{
      return res.status(200).json({
        id: subscription[0].idSubscription,
        name: subscription[0].name,
        description: subscription[0].description,
        cost: subscription[0].cost,
        duration: subscription[0].duration
      })
    })
    .catch(err=>{
      console.log(err);
      res.status(500).json({error: err});
  })
};