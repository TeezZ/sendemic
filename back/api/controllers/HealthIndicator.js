const HealthIndicator = require('../models/HealthIndicator');
const WorkerIndicator = require('../models/WorkerIndicator');

exports.HealthIndicator_create = (req, res, next) => {

    const date = req.body.date;
    const temperature = req.body.temperature;
    const saturation = req.body.saturation;
    const idWorker = req.body.id_worker;

    HealthIndicator.create({
        date: date,
        temperature: temperature,
        saturation: saturation,
    }).then(result => {
        console.log(result);
        HealthIndicator.findAll({
            where:
            {
                date: date,
                temperature: temperature,
                saturation: saturation
            }, raw: true
        }).then(indicator => {
            WorkerIndicator.create({
                fk_worker_id: idWorker,
                fk_indicator_id: indicator[0].id
            }).then(result => {
                console.log(result);
                res.status(201).json({ message: "HealthIndicator created!" });
            })
        })
    }
    ).catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
    });

};

exports.HealthIndicator_getByWorkerId = (req, res, next) => {

    const workerId = req.body.id_worker;

    try {
        let query = 'SELECT id, date, temperature, saturation FROM health_indicators WHERE id IN (SELECT fk_indicator_id FROM worker_indicators WHERE fk_worker_id = ' + workerId + ') ORDER BY id DESC';
        sequelize
            .query(query, {
                type: sequelize.QueryTypes.SELECT
            })
            .then(row => {
                res.status(200).json({
                    Indicators: row
                });
            })
    }
    catch (err) {
        res.status(400).json({
            message: err
        });
        console.log(err);
    }

};
