const express = require('express');
const router = express.Router();

const HeadMaster = sequelize.define("headmaster", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    first_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    last_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    passwordHash: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Worker = sequelize.define("worker", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    first_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    last_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    position: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Company = sequelize.define("company", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    subscription: {
        type: Sequelize.STRING,
        allowNull: false
    },
    subscription_start_date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    subscription_end_date: {
        type: Sequelize.DATE,
        allowNull: false
    }
});

const Subscription = sequelize.define("subscription", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    cost: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    duration: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
});

const HealthIndicator = sequelize.define("health_indicator", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    temperature: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    saturation: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

const WorkerIndicator = sequelize.define("worker_indicator", {
});

const HeadmasterCompany = sequelize.define("worker_indicator", {
});

Worker.hasMany(WorkerIndicator, { onDelete: "cascade" });
HealthIndicator.hasMany(WorkerIndicator, { onDelete: "cascade" });

Headmaster.hasMany(HeadmasterCompany, { onDelete: "cascade" });
Company.hasMany(HeadmasterCompany, { onDelete: "cascade" });

Company.hasMany(Worker, { onDelete: "cascade", foreignKey: "id", sourceKey: "id" });
Worker.belongsTo(Company, { onDelete: "cascade", foreignKey: "id", targetKey: "id" });

Subscription.hasMany(Company, { onDelete: "cascade", foreignKey: "id", sourceKey: "id" });
Company.belongsTo(Subscription, { onDelete: "cascade", foreignKey: "id", targetKey: "id" });

sequelize.sync();

module.exports.HeadMaster = HeadMaster;
module.exports.HeadmasterCompany = HeadmasterCompany;
module.exports.Company = Company;
module.exports.Subscription = Subscription;
module.exports.Worker = Worker;
module.exports.WorkerIndicator = WorkerIndicator;
module.exports.HealthIndicator = HealthIndicator;