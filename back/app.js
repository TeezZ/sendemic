const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");

const headmasterRoutes = require("./api/routes/Headmaster");
const administratorRoutes = require("./api/routes/Administrator");
const companyRoutes = require("./api/routes/Company");
const subscriptionRoutes = require("./api/routes/Subscription");
const workerRoutes = require("./api/routes/Worker");
const healthIndicatorRoutes = require("./api/routes/HealthIndicator");

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
        return res.status(200).json({});
    }
    next();
});
 
//links build
require("./api/database/tablesConnections")();

//Default filling table. Use it once after droping DB
//// require('./api/database/defaultFilling');
//

//Routes which should handle requests
app.use("/headmasters", headmasterRoutes);
app.use("/administrators", administratorRoutes);
app.use("/companies", companyRoutes);
app.use("/subscriptions", subscriptionRoutes);
app.use("/workers", workerRoutes);
app.use("/healthindicators", healthIndicatorRoutes);



app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;