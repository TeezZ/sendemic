$(document).ready(function() {
    let language = $('html')[0].lang;

    $('#login').submit(function(e) {
        e.preventDefault();

        $('#error').empty();

        let email = $('input[name="email__inp"]').val();
        let password = $('input[name="password__inp"]').val();

        $.post('https://sendemic.herokuapp.com/headmasters/login', {
                    email: email,
                    password: password
                },
                function(data) {
                    console.log(data);
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('type', 'admin');

                    switch (language) {
                        case 'en':
                            $(window).attr('location', '../en/profile.html');
                            break;
                        case 'ua':
                            $(window).attr('location', '../ua/profile.html');
                            break;
                    }

                }, 'json')
            .fail(function(xhr, ajaxOptions, thrownError) {
                console.log("error");
                console.log(xhr.status);
                console.log(thrownError);
                console.log(xhr.responseText);
                switch (xhr.status) {
                    case 401:
                        switch (language) {
                            case 'en':
                                $('.error').text('Invalid email or password');
                                break;
                            case 'ua':
                                $('.error').text('Невірна пошта чи пароль');
                                break;
                        }
                        return false;
                        break;
                    case 0:
                        switch (language) {
                            case 'en':
                                $('.error').text('The server is offline');
                                break;
                            case 'ua':
                                $('.error').text('Сервер не працює');
                                break;
                        }

                        return false;
                        break;
                }
            });
    });
});