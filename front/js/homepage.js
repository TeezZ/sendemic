let token = localStorage.getItem('token');
let type = localStorage.getItem('type')
let userLastName, userId;
let subscriptions = [];

$(document).ready(function() {
    let language = $('html')[0].lang;

    $.ajax({
        type: 'GET',
        url: 'https://sendemic.herokuapp.com/subscriptions/getSubscriptions',
        contentType: 'application/json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", 'Bearer ' + token);
        },
        dataType: 'json',
        success: function(data) {
            subscriptions = data.allSubscriptions;
            for (let i = 0; i < subscriptions.length; i++) {
                $('.subscription_block').append(`
                        <div class="subscription_card">
                            <h4 class="subscription_title">${subscriptions[i].name}</h4>
                            <div class="subscription_desc">${subscriptions[i].description}</div>
                            <div class="subscription_cost">${subscriptions[i].cost} $</div>
                            <div class="subscription_duration">${subscriptions[i].duration} days</div>
                            <a href="./login.html">
                                <div class="subscribe_button">subscribe</div>
                            </a>
                        </div>
                    `);
            }
        },
        error: function(jqXHR, exception) {
            switch (jqXHR.status) {
                case 403:
                    localStorage.clear();
                    $(window).attr('location', './homepage.html')
                    break;
            }
        }
    });


    $('.logout').click(function() {
        localStorage.clear();
    });

});