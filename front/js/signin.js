$(document).ready(function() {
    let language = $('html')[0].lang;

    $('#signin').submit(function(e) {
        e.preventDefault();
        $('#error').empty();

        let first_name = $('input[name="first_name__inp"]').val().toLowerCase().replace(/\b[a-z]/g, function(txtVal) {
            return txtVal.toUpperCase();
        });
        let last_name = $('input[name="last_name__inp"]').val().toLowerCase().replace(/\b[a-z]/g, function(txtVal) {
            return txtVal.toUpperCase();
        });
        let email = $('input[name="email__inp"]').val();
        let password = $('input[name="password__inp"]').val();
        let confirmPassword = $('input[name="confirm_password__inp"]').val();

        if (password != confirmPassword) {
            switch (language) {
                case 'en':
                    $('.error').text('Passwords do not match');
                    break;
                case 'ua':
                    $('.error').text('Паролі не співпадають');
                    break;
            }
        } else {
            $.post('https://sendemic.herokuapp.com/headmasters/', {
                        first_name: first_name,
                        last_name: last_name,
                        email: email,
                        password: password
                    },
                    function(data) {
                        console.log(data);
                        switch (language) {
                            case 'en':
                                $('.error').text('Account created! Now you can login!');
                                break;
                            case 'ua':
                                $('.error').text('Обліковий запис створено! Тепер Ви можете увійти до системи!');
                                break;
                        }
                    }, 'json')
                .fail(function(xhr, ajaxOptions, thrownError) {
                    console.log("error");
                    switch (language) {
                        case 'en':
                            $('.error').text('Something went wrong!');
                            break;
                        case 'ua':
                            $('.error').text('Щось пішло не так!');
                            break;
                    }
                    console.log(xhr.status);
                    console.log(thrownError);
                    console.log(xhr.responseText);
                    switch (xhr.status) {
                        case 0:
                            switch (language) {
                                case 'en':
                                    $('.error').text('The server is offline');
                                    break;
                                case 'ua':
                                    $('.error').text('Сервер не працює');
                                    break;
                            }
                            return false;
                            break;
                    }
                });
        }
    });

});