 let token = localStorage.getItem('token');
 let type = localStorage.getItem('type')
 let userName, userId;

 $.ajax({
     type: 'GET',
     url: 'https://sendemic.herokuapp.com/administrators/getAdministratorProfile',
     contentType: 'application/json',
     beforeSend: function(xhr) {
         xhr.setRequestHeader("Authorization", 'Bearer ' + token);
     },
     dataType: 'json',
     success: function(data) {
         userName = data.email;
         userId = data.id;
         $(".small_profile").empty();
         $(".small_profile").html(`
                <a>
                    <div class="small_profile_info">
                    <div class="profile_name">${userName}</div>
                    <div class="profile_id">id: ${userId}</div>
                </a>
                <a class="logout" title="logout" href="../en/admin_panel_login.html">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            `);

     },
     error: function(jqXHR, exception) {
         switch (jqXHR.status) {
             case 403:
                 localStorage.clear();
                 switch (language) {
                     case 'en':
                         $(window).attr('location', '../en/admin_panel_login.html');
                         break;
                     case 'ua':
                         $(window).attr('location', '../ua/admin_panel_login.html');
                         break;
                 }
                 break;
         }
     }
 });


 $(document).ready(function() {
     let language = $('html')[0].lang;

     $('.logout').click(function() {
         localStorage.clear();
     });

     $('#create_subscription__form').submit(function(e) {
         e.preventDefault();

         let name = $('input[name="name__inp"]').val();
         let description = $('input[name="description__inp"]').val();
         let cost = $('input[name="cost__inp"]').val();
         let duration = 30;

         $.ajaxSetup({
             beforeSend: function(xhr) {
                 xhr.setRequestHeader('Authorization', token);
             }
         });

         $.post('https://sendemic.herokuapp.com/subscriptions/', {
                     name: name,
                     description: description,
                     cost: cost,
                     duration: duration
                 },
                 function(data) {
                     console.log(data);
                     switch (language) {
                         case 'en':
                             $('.error').text('Subscription created!');
                             break;
                         case 'ua':
                             $('.error').text('Підписка створена!');
                             break;
                     }
                     $('input[name="name__inp"]').val('');
                     $('input[name="description__inp"]').val('');
                     $('input[name="cost__inp"]').val('');
                     $('input[name="duration__inp"]').val('');
                 }, 'json')
             .fail(function(xhr, ajaxOptions, thrownError) {
                 console.log("error");
                 $('.error').text('Something went wrong');
                 console.log(xhr.status);
                 console.log(thrownError);
                 console.log(xhr.responseText);
                 switch (xhr.status) {
                     case 0:
                         switch (language) {
                             case 'en':
                                 $('.error').text('The server is offline');
                                 break;
                             case 'ua':
                                 $('.error').text('Сервер не працює');
                                 break;
                         }
                         return false;
                         break;
                 }
             });
     });

     $('#subscription_clear').on('click', function(e) {
         e.preventDefault();
         $('input[name="name__inp"]').val('');
         $('input[name="description__inp"]').val('');
         $('input[name="cost__inp"]').val('');
         $('input[name="duration__inp"]').val('');
     });


     $.ajax({
         type: 'GET',
         url: 'https://sendemic.herokuapp.com/subscriptions/getSubscriptions',
         contentType: 'application/json',
         beforeSend: function(xhr) {
             xhr.setRequestHeader("Authorization", 'Bearer ' + token);
         },
         dataType: 'json',
         success: function(data) {
             subscriptions = data.allSubscriptions;
             for (let i = 0; i < subscriptions.length; i++) {
                 $('.sub_radio').append(`
                        <input class="checkbox-tools" type="radio" name="tools" id="${subscriptions[i].id}" checked>
                        <label class="for-checkbox-tools" for="${subscriptions[i].id}">
                             <h4 class="subscription_title">${subscriptions[i].name}</h4>
                                    <div class="subscription_desc">${subscriptions[i].description}</div>
                                    <div class="subscription_cost">${subscriptions[i].cost} $</div>
                                    <div class="subscription_duration">${subscriptions[i].duration} days</div>
                    `);
             }


             $("#delete_subscribtion_btn").on("click", function() {
                 let subscription_id = $('input[type=radio][name=tools]:checked').attr('id');

                 $.ajaxSetup({
                     beforeSend: function(xhr) {
                         xhr.setRequestHeader('Authorization', token);
                     }
                 });

                 $.post('https://sendemic.herokuapp.com/subscriptions/deleteSubscriptionById', {
                             subscription_id: subscription_id
                         },
                         function(data) {
                             console.log(data);
                             switch (language) {
                                 case 'en':
                                     $('.error').text('Subscribtion deleted!');
                                     break;
                                 case 'ua':
                                     $('.error').text('Підписку видалено!');
                             }
                             location.reload();
                         }, 'json')
                     .fail(function(xhr, ajaxOptions, thrownError) {
                         console.log("error");
                         $('.error').text('Something went wrong');
                         console.log(xhr.status);
                         console.log(thrownError);
                         console.log(xhr.responseText);
                         switch (xhr.status) {
                             case 0:
                                 switch (language) {
                                     case 'en':
                                         $('.error').text('The server is offline');
                                         break;
                                     case 'ua':
                                         $('.error').text('Сервер не працює');
                                         return false;
                                         break;
                                 }
                         }
                     });
             });
         },
         error: function(jqXHR, exception) {
             switch (jqXHR.status) {
                 case 403:
                     localStorage.clear();
                     $(window).attr('location', './admin_login.js')
                     break;
             }
         }
     });

 });