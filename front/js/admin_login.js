$(document).ready(function() {
    let language = $('html')[0].lang;

    $('#admin_login').submit(function(e) {
        e.preventDefault();

        let email = $('input[name="email__inp"]').val();
        let password = $('input[name="password__inp"]').val();

        $.post('https://sendemic.herokuapp.com/administrators/login', {
                    email: email,
                    password: password
                },
                function(data) {
                    console.log(data);
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('type', 'admin');

                    switch (language) {
                        case 'en':
                            $(window).attr('location', '../en/admin_panel.html')
                            break;
                        case 'ua':
                            $(window).attr('location', '../ua/admin_panel.html')
                            break;
                    }

                }, 'json')
            .fail(function(xhr, ajaxOptions, thrownError) {
                console.log("error");
                console.log(xhr.status);
                console.log(thrownError);
                console.log(xhr.responseText);
                switch (xhr.status) {
                    case 401:
                        $('.error').text('Invalid email or password');
                        return false;
                        break;
                    case 0:
                        $('.error').text('The server is offline');
                        return false;
                        break;
                }
            });
    });
});