let token = localStorage.getItem('token');
let type = localStorage.getItem('type');
let userFirstName, userLastName, userId, userEmail;

$(document).ready(function() {
    let language = $('html')[0].lang;

    var ctx = $('#company_chart');
    // var myChart = new Chart(ctx, {
    //     type: 'line',
    //     data: {
    //         labels: ['05-11-2020', '05-11-2020', '05-12-2020', '05-12-2020', '05-13-2020', '05-13-2020'],
    //         datasets: [{
    //             label: 'Workers tempetarure',
    //             data: [36.6, 37.1, 38, 37.9, 37.5, 38.1],
    //             backgroundColor: [
    //                 'rgba(255, 99, 132, 0.2)',
    //             ],
    //             borderColor: 'rgba(255, 99, 132, 1)',
    //             borderWidth: 1
    //         }]
    //     },
    //     options: {
    //         scales: {
    //             yAxes: [{
    //                 ticks: {
    //                     beginAtZero: false
    //                 }
    //             }]
    //         }
    //     }
    // });

    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', token);
        }
    });

    $.ajax({
        type: 'GET',
        url: 'https://sendemic.herokuapp.com/headmasters/getHeadmasterProfile',
        contentType: 'application/json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", 'Bearer ' + token);
        },
        dataType: 'json',
        success: function(data) {
            userId = data.id;
            userFirstName = data.first_name;
            userLastName = data.last_name;
            userEmail = data.email;

            $(".small_profile").empty();
            $(".small_profile").html(`
                <a href="./profile.html" title="go to profile">
                    <div class="small_profile_info">
                    <div class="profile_name">${userLastName}</div>
                    <div class="profile_id">id: ${userId}</div>
                </a>
                <a class="logout" title="logout" href="./homepage.html">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            `);

            $('#profile_id').html(userId);
            $('#profile_first_name').html(userFirstName);
            $('#profile_last_name').html(userLastName);
            $('#profile_email').html(userEmail);

            $.post('https://sendemic.herokuapp.com/companies/getCompanyByHeadmasterId', {
                        headmasterId: userId
                    },
                    function(data) {
                        let companies = data.HeadmasterCompanies;

                        if (companies.length > 0) {
                            $('.add_company').empty();
                            switch (language) {
                                case 'en':
                                    $('.company_info').html(`
                                        <div class="profile_information_header">About your company</div>
                                        <div class="info_field" class="company_id">
                                            <div>Company ID:</div>
                                            <div id="profile_company_id">${companies[0].id}</div>
                                        </div>
                                        <div class="info_field" class="profile_company_name">
                                            <div>Name:</div>
                                            <div id="profile_company_name">${companies[0].name}</div>
                                        </div>
                                        <div class="info_field" class="profile_company_address">
                                            <div>Address:</div>
                                            <div id="company_address">${companies[0].address}</div>
                                        </div>
                                        <div class="info_field" class="profile_company_subscription">
                                            <div>Subscription:</div>
                                            <div id="subscription">${companies[0].subscription_id}</div>
                                        </div>
                                        `);
                                    $('.add_worker').html(`
                                        <a href="./addworker.html">
                                        <div class="accent_btn">add worker</div>
                                        </a>`);
                                    break;

                                case 'ua':
                                    $('.company_info').html(`
                                        <div class="profile_information_header">Про Вашу компанію</div>
                                        <div class="info_field" class="company_id">
                                            <div>ID компанії:</div>
                                            <div id="profile_company_id">${companies[0].id}</div>
                                        </div>
                                        <div class="info_field" class="profile_company_name">
                                            <div>Назва:</div>
                                            <div id="profile_company_name">${companies[0].name}</div>
                                        </div>
                                        <div class="info_field" class="profile_company_address">
                                            <div>Адреса:</div>
                                            <div id="company_address">${companies[0].address}</div>
                                        </div>
                                        <div class="info_field" class="profile_company_subscription">
                                            <div>Підписка:</div>
                                            <div id="subscription">${companies[0].subscription_id}</div>
                                        </div>
                                        `);
                                    $('.add_worker').html(`
                                            <a href="./addworker.html">
                                            <div class="accent_btn">додати працівника</div>
                                        </a>`);
                                    break;
                            }
                        }

                        console.log(companies.length);
                        if (companies.length == 0) {
                            switch (language) {
                                case 'en':
                                    $('#attention').text("You haven't created a company yet or you don't have 3 days' health records");
                                    break;
                                case 'ua':
                                    $('#attention').text("Ви ще не створили компанію або ще немає записів показників здоров'я хоча б за 3 дні");
                                    break;
                            }
                        }
                        $.post('https://sendemic.herokuapp.com/healthindicators/getCompanyHealthIndicators', {
                                    company_id: companies[0].id
                                },
                                function(data) {
                                    console.log(data);
                                    let body = data.HealthCompany;
                                    let indicatorsDates = [];
                                    let indicatorsTemperature = [];

                                    let attentionEn = {
                                        good: "Indicators are good!",
                                        risk: "Look out! There's a risk of infection spreading"
                                    };

                                    let attentionUa = {
                                        good: "Стан компанії задовільний! Так тримати!",
                                        risk: "Обережно! Існує риск росповсюдження інфекції!"
                                    };

                                    for (let i = 0; i < body.length; i++) {
                                        let date = body[i].date.split('T');
                                        indicatorsDates.push(date[0]);
                                        indicatorsTemperature.push(body[i].temperature);
                                    }

                                    let attentionTemperature = 36.6;
                                    if (indicatorsTemperature.length >= 3) {
                                        let sum = 1;
                                        for (let i = indicatorsTemperature.length - 1; i > indicatorsTemperature.length - 4; i--) {
                                            sum += indicatorsTemperature[i];
                                        }

                                        attentionTemperature = sum / 3;
                                    }

                                    switch (language) {
                                        case 'en':

                                            if (attentionTemperature > 37.5) {
                                                $('#attention').text(attentionEn.risk)
                                            } else {
                                                $('#attention').text(attentionEn.good)
                                            }
                                            var myChart = new Chart(ctx, {
                                                type: 'line',
                                                data: {
                                                    labels: indicatorsDates,
                                                    datasets: [{
                                                        label: 'Workers temperature',
                                                        data: indicatorsTemperature,
                                                        backgroundColor: [
                                                            'rgba(255, 99, 132, 0.2)',
                                                        ],
                                                        borderColor: 'rgba(255, 99, 132, 1)',
                                                        borderWidth: 1
                                                    }]
                                                },
                                                options: {
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero: false
                                                            }
                                                        }]
                                                    }
                                                }
                                            });
                                            break;
                                        case 'ua':
                                            if (attentionTemperature > 37.5) {
                                                $('#attention').text(attentionUa.risk)
                                            } else {
                                                $('#attention').text(attentionUa.good)
                                            }
                                            var myChart = new Chart(ctx, {
                                                type: 'line',
                                                data: {
                                                    labels: indicatorsDates,
                                                    datasets: [{
                                                        label: 'Температура працівників',
                                                        data: indicatorsTemperature,
                                                        backgroundColor: [
                                                            'rgba(255, 99, 132, 0.2)',
                                                        ],
                                                        borderColor: 'rgba(255, 99, 132, 1)',
                                                        borderWidth: 1
                                                    }]
                                                },
                                                options: {
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero: false
                                                            }
                                                        }]
                                                    }
                                                }
                                            });
                                            break;
                                    }

                                }, 'json')
                            .fail(function(xhr, ajaxOptions, thrownError) {
                                console.log("error");
                                console.log(xhr.status);
                                console.log(thrownError);
                                console.log(xhr.responseText);
                                switch (xhr.status) {
                                    case 401:
                                        switch (language) {
                                            case 'en':
                                                $('.error').text('Invalid email or password');
                                                break;
                                            case 'ua':
                                                $('.error').text('Невірна пошта чи пароль');
                                                break;
                                        }
                                        return false;
                                        break;
                                    case 0:
                                        switch (language) {
                                            case 'en':
                                                $('.error').text('The server is offline');
                                                break;
                                            case 'ua':
                                                $('.error').text('Сервер не працює');
                                                break;
                                        }

                                        return false;
                                        break;
                                }
                            });

                    }, 'json')
                .fail(function(xhr, ajaxOptions, thrownError) {
                    console.log("error");
                    $('.error').text('Something went wrong');
                    console.log(xhr.status);
                    console.log(thrownError);
                    console.log(xhr.responseText);
                    switch (xhr.status) {
                        case 0:
                            switch (language) {
                                case 'en':
                                    $('.error').text('The server is offline');
                                    break;
                                case 'ua':
                                    $('.error').text('Сервер не працює');
                                    break;
                            }
                            return false;
                            break;
                    }
                });
        },
        error: function(jqXHR, exception) {
            switch (jqXHR.status) {
                case 403:
                    localStorage.clear();
                    $(window).attr('location', './homepage.html')
                    break;
            }
        }
    });

    $('.logout').click(function() {
        localStorage.clear();
    });

    $('#worker_search_btn').click(function() {
        localStorage.setItem('workers_search', $('input[name="search_text__inp"]').val());
        $(window).attr('location', './workers_search.html');
    });

});