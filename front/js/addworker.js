let token = localStorage.getItem('token');
let type = localStorage.getItem('type')
let userLastName, userId;

$(document).ready(function() {
    let language = $('html')[0].lang;

    $.ajax({
        type: 'GET',
        url: 'https://sendemic.herokuapp.com/headmasters/getHeadmasterProfile',
        contentType: 'application/json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", 'Bearer ' + token);
        },
        dataType: 'json',
        success: function(data) {
            userId = data.id;
            userLastName = data.last_name;

            $(".small_profile").empty();
            $(".small_profile").html(`
                <a href="./profile.html" title="go to profile">
                    <div class="small_profile_info">
                    <div class="profile_name">${userLastName}</div>
                    <div class="profile_id">id: ${userId}</div>
                </a>
                <a class="logout" title="logout" href="./homepage.html">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            `);
        },
        error: function(jqXHR, exception) {
            switch (jqXHR.status) {
                case 403:
                    localStorage.clear();
                    $(window).attr('location', './homepage.html')
                    break;
            }
        }
    });

    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', token);
        }
    });

    $('#company_creation').submit(function(e) {
        e.preventDefault();
        
        $.post('https://sendemic.herokuapp.com/companies/getCompanyByHeadmasterId', {
                    headmasterId: userId
                },
                function(data) {
                    let companies = data.HeadmasterCompanies;

                    let first_name = $('input[name="worker_first_name__inp"]').val();
                    let last_name = $('input[name="worker_last_name__inp"]').val();
                    let position = $('input[name="position__inp"]').val();

                    $.post('https://sendemic.herokuapp.com/workers/', {
                                company_id: companies[0].id,
                                first_name: first_name,
                                last_name: last_name,
                                position: position
                            },
                            function(data) {
                                console.log(data);
                                $('input[name="worker_first_name__inp"]').val('');
                                $('input[name="worker_last_name__inp"]').val('');
                                $('input[name="position__inp"]').val('');
                                switch (language) {
                                    case 'en':
                                        $('.error').text('Worker added!');
                                        break;
                                    case 'ua':
                                        $('.error').text('Працівник доданий!');
                                }

                            }, 'json')
                        .fail(function(xhr, ajaxOptions, thrownError) {
                            console.log("error");
                            $('.error').text('Something went wrong');
                            console.log(xhr.status);
                            console.log(thrownError);
                            console.log(xhr.responseText);
                            switch (xhr.status) {
                                case 0:
                                    switch (language) {
                                        case 'en':
                                            $('.error').text('The server is offline');
                                            break;
                                        case 'ua':
                                            $('.error').text('Сервер не працює');
                                            return false;
                                            break;
                                    }
                            }
                        });


                }, 'json')
            .fail(function(xhr, ajaxOptions, thrownError) {
                console.log("error");
                $('.error').text('Something went wrong');
                console.log(xhr.status);
                console.log(thrownError);
                console.log(xhr.responseText);
                switch (xhr.status) {
                    case 0:
                        switch (language) {
                            case 'en':
                                $('.error').text('The server is offline');
                                break;
                            case 'ua':
                                $('.error').text('Сервер не працює');
                                break;
                        }
                        return false;
                        break;
                }
            });
    });

    $('.logout').click(function() {
        localStorage.clear();
    });

});