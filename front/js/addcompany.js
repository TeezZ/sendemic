let token = localStorage.getItem('token');
let type = localStorage.getItem('type')
let userLastName, userId;
let subscriptions = [];

$(document).ready(function() {
    let language = $('html')[0].lang;

    $.ajax({
        type: 'GET',
        url: 'https://sendemic.herokuapp.com/headmasters/getHeadmasterProfile',
        contentType: 'application/json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", 'Bearer ' + token);
        },
        dataType: 'json',
        success: function(data) {
            userId = data.id;
            userLastName = data.last_name;

            $(".small_profile").empty();
            $(".small_profile").html(`
                <a href="./profile.html" title="go to profile">
                    <div class="small_profile_info">
                    <div class="profile_name">${userLastName}</div>
                    <div class="profile_id">id: ${userId}</div>
                </a>
                <a class="logout" title="logout" href="./homepage.html">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            `);
        },
        error: function(jqXHR, exception) {
            switch (jqXHR.status) {
                case 403:
                    localStorage.clear();
                    $(window).attr('location', './homepage.html')
                    break;
            }
        }
    });

    $.ajax({
        type: 'GET',
        url: 'https://sendemic.herokuapp.com/subscriptions/getSubscriptions',
        contentType: 'application/json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", 'Bearer ' + token);
        },
        dataType: 'json',
        success: function(data) {
            subscriptions = data.allSubscriptions;
            for (let i = 0; i < subscriptions.length; i++) {
                $('.sub_radio').append(`
                        <input class="checkbox-tools" type="radio" name="tools" id="${subscriptions[i].id}" checked>
                        <label class="for-checkbox-tools" for="${subscriptions[i].id}">
                             <h4 class="subscription_title">${subscriptions[i].name}</h4>
                                    <div class="subscription_desc">${subscriptions[i].description}</div>
                                    <div class="subscription_cost">${subscriptions[i].cost} $</div>
                                    <div class="subscription_duration">${subscriptions[i].duration} days</div>
                    `);
            }


            $("#enter_focus_btn").on("click", function() {
                let name = $('input[name="company_name__inp"]').val();
                let address = $('input[name="address__inp"]').val();
                let subscription_id = $('input[type=radio][name=tools]:checked').attr('id');

                let date = new Date();
                let subscription_start = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                let duration = 30;
                for (let i = 0; i < subscriptions.length; i++) {
                    if (subscriptions[i].id == subscription_id) {
                        duration = subscriptions[i].duration;
                        break;
                    };
                }

                date.setDate(date.getDate() + duration);
                let subscription_end = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();

                $.ajaxSetup({
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Authorization', token);
                    }
                });
                
                $.post('https://sendemic.herokuapp.com/companies/', {
                            headmaster_id: userId,
                            name: name,
                            address: address,
                            subscription_id: subscription_id,
                            subscription_start: subscription_start,
                            subscription_end: subscription_end
                        },
                        function(data) {
                            console.log(data);
                            switch (language) {
                                case 'en':
                                    $('.error').text('Company added!');
                                    break;
                                case 'ua':
                                    $('.error').text('Компанія успішно додана!');
                            }

                        }, 'json')
                    .fail(function(xhr, ajaxOptions, thrownError) {
                        console.log("error");
                        
                        switch (language) {
                                    case 'en':
                                        $('.error').text('Something went wrong');
                                        break;
                                    case 'ua':
                                       $('.error').text('Щось пішло не так');
                                        return false;
                                        break;
                                }
                        console.log(xhr.status);
                        console.log(thrownError);
                        console.log(xhr.responseText);
                        switch (xhr.status) {
                            case 0:
                                switch (language) {
                                    case 'en':
                                        $('.error').text('The server is offline');
                                        break;
                                    case 'ua':
                                        $('.error').text('Сервер не працює');
                                        return false;
                                        break;
                                }
                        }
                    });
            });
        },
        error: function(jqXHR, exception) {
            switch (jqXHR.status) {
                case 403:
                    localStorage.clear();
                    $(window).attr('location', './homepage.html')
                    break;
            }
        }
    });


    $('.logout').click(function() {
        localStorage.clear();
    });

});