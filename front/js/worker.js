var ctx = $('#worker_chart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['05-11-2020', '05-11-2020', '05-12-2020', '05-12-2020', '05-13-2020', '05-13-2020'],
        datasets: [{
            label: 'Tempetarure',
            data: [36.6, 37.1, 38, 37.9, 37.5, 38.1],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: false
                }
            }]
        }
    }
});