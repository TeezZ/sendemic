let token = localStorage.getItem('token');
let type = localStorage.getItem('type');
let workersSearch = localStorage.getItem('workers_search');
let userLastName, userId;

$(document).ready(function() {
    let language = $('html')[0].lang;

    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', token);
        }
    });

    $.ajax({
        type: 'GET',
        url: 'https://sendemic.herokuapp.com/headmasters/getHeadmasterProfile',
        contentType: 'application/json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", 'Bearer ' + token);
        },
        dataType: 'json',
        success: function(data) {
            userId = data.id;
            userLastName = data.last_name;

            $(".small_profile").empty();
            $(".small_profile").html(`
                <a href="./profile.html" title="go to profile">
                    <div class="small_profile_info">
                    <div class="profile_name">${userLastName}</div>
                    <div class="profile_id">id: ${userId}</div>
                </a>
                <a class="logout" title="logout" href="./homepage.html">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            `);

            $.post('https://sendemic.herokuapp.com/companies/getCompanyByHeadmasterId', {
                        headmasterId: userId
                    },
                    function(data) {
                        let companies = data.HeadmasterCompanies;

                        console.log(workersSearch);
                        console.log(companies[0].id);

                        $.post('https://sendemic.herokuapp.com/workers/getWorkerByName', {
             last_name: workersSearch,
             company_id: companies[0].id
         },
         function(data) {
             let workers = data.Workers;

             for (let i = 0; i < workers.length; i++) {
                 $('.workers_search_table tr:last').after(`
                                     <tr class="accent_btn" id="${workers[i].id}" class="worker_row">
                                     <td align="center">${workers[i].id}</td>
                                     <td align="center">${workers[i].first_name}</td>
                                     <td align="center">${workers[i].last_name}</td>
                                     <td align="center">${workers[i].position}</td>
                                 </tr>
                                    		`);
             }

         }, 'json')
     .fail(function(xhr, ajaxOptions, thrownError) {
         console.log("error");
         $('.error').text('Something went wrong');
         console.log(xhr.status);
         console.log(thrownError);
         console.log(xhr.responseText);
         switch (xhr.status) {
             case 0:
                 switch (language) {
                     case 'en':
                         $('.error').text('The server is offline');
                         break;
                     case 'ua':
                         $('.error').text('Сервер не працює');
                         return false;
                         break;
                 }
         }
     });

                    }, 'json')
                .fail(function(xhr, ajaxOptions, thrownError) {
                    console.log("error");
                    $('.error').text('Something went wrong');
                    console.log(xhr.status);
                    console.log(thrownError);
                    console.log(xhr.responseText);
                    switch (xhr.status) {
                        case 0:
                            switch (language) {
                                case 'en':
                                    $('.error').text('The server is offline');
                                    break;
                                case 'ua':
                                    $('.error').text('Сервер не працює');
                                    break;
                            }
                            return false;
                            break;
                    }
                });

        },
        error: function(jqXHR, exception) {
            switch (jqXHR.status) {
                case 403:
                    localStorage.clear();
                    $(window).attr('location', './homepage.html')
                    break;
            }
        }
    });

    $('#worker_search_btn').click(function() {
        localStorage.setItem('workers_search', $('input[name="search_text__inp"]').val());
        $(window).attr('location', './workers_search.html');
    });


    $('.logout').click(function() {
        localStorage.clear();
    });

});